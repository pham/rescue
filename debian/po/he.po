# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_he.po to Hebrew
# Hebrew messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Translations from iso-codes:
# Translations taken from ICU SVN on 2007-09-09
# Translations taken from KDE:
# Amit Dovev <debian.translator@gmail.com>, 2007.
# Meital Bourvine <meitalbourvine@gmail.com>, 2007.
# Omer Zak <w1@zak.co.il>, 2008, 2010, 2012, 2013.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Free Software Foundation, Inc., 2002,2004.
# Alastair McKinstry <mckinstry@computer.org>, 2002.
# Meni Livne <livne@kde.org>, 2000.
# Free Software Foundation, Inc., 2002,2003.
# Meni Livne <livne@kde.org>, 2000.
# Meital Bourvine <meitalbourvine@gmail.com>, 2007.
# Lior Kaplan <kaplan@debian.org>, 2004-2007, 2008, 2010, 2011, 2015, 2017.
# Yaron Shahrabani <sh.yaron@gmail.com>, 2018, 2019, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: rescue@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:37+0100\n"
"PO-Revision-Date: 2020-07-26 03:41+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew (https://www.transifex.com/yaron/teams/79473/he/)\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: title
#. Description
#. Info message displayed when running in rescue mode
#. :sl2:
#: ../rescue-check.templates:2001
msgid "Rescue mode"
msgstr "מצב הצלה"

#. Type: text
#. Description
#. :sl1:
#: ../rescue-mode.templates:1001
msgid "Enter rescue mode"
msgstr "כניסה למצב הצלה"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid "No partitions found"
msgstr "לא נמצאו מחיצות"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:2001
msgid ""
"The installer could not find any partitions, so you will not be able to "
"mount a root file system. This may be caused by the kernel failing to detect "
"your hard disk drive or failing to read the partition table, or the disk may "
"be unpartitioned. If you wish, you may investigate this from a shell in the "
"installer environment."
msgstr ""
"מנהל ההתקנה לא הצליח למצוא אף מחיצה, כך שלא ניתן לעגון שורש מערכת קבצים. "
"סיבות אפשריות לכך הן, שהליבה לא גילתה את הכונן הקשיח שלך או לא הצליחה לקרוא "
"את טבלת המחיצות או שהכונן לא חולק למחיצות. לרשותך מסוף בסביבת ההתקנה אם יש "
"לך עניין לחקור את הנושא הזה."

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Assemble RAID array"
msgstr "הרכבת מערך RAID"

#. Type: select
#. Choices
#. :sl3:
#: ../rescue-mode.templates:3001
msgid "Do not use a root file system"
msgstr "לא להשתמש במערכת קבצים כתיקיית שורש"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid "Device to use as root file system:"
msgstr "התקן לשימוש כשורש מערכת הקבצים:"

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"Enter a device you wish to use as your root file system. You will be able to "
"choose among various rescue operations to perform on this file system."
msgstr ""
"נא להקליד שם של התקן שישמש אותך כשורש מערכת הקבצים. לרשותך תעמודנה מגוון "
"פעולות לחילוץ מערכת הקבצים הזו."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:3002
msgid ""
"If you choose not to use a root file system, you will be given a reduced "
"choice of operations that can be performed without one. This may be useful "
"if you need to correct a partitioning problem."
msgstr ""
"בחירה שלא להשתמש בשורש מערכת הקבצים תוביל לצמצום מגוון הפעולות שניתן לבצע "
"ללא גישה אליו. עשוי להיות שימושי לטובת תיקון תקלת חלוקה למחיצות."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid "No such device"
msgstr "לא קיים התקן"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:4001
msgid ""
"The device you entered for your root file system (${DEVICE}) does not exist. "
"Please try again."
msgstr ""
"ההתקן (${DEVICE}), שהוכנס בשביל שורש מערכת הקבצים לא קיים. נא להכניס התקן "
"אחר."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Mount failed"
msgstr "עגינה נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid ""
"An error occurred while mounting the device you entered for your root file "
"system (${DEVICE}) on /target."
msgstr ""
"התרחשה שגיאה בזמן עגינת ההתקן שנבחר כשורש מערכת הקבצים (${DEVICE}) על ‎/"
"target."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:5001
msgid "Please check the syslog for more information."
msgstr "בדוק את התיעוד המערכת (syslog) בשביל מידע נוסף."

#. Type: select
#. Description
#. :sl3:
#: ../rescue-mode.templates:6001
msgid "Rescue operations"
msgstr "פעולות הצלה"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "Rescue operation failed"
msgstr "פעולת הצלה נכשלה"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:7001
msgid "The rescue operation '${OPERATION}' failed with exit code ${CODE}."
msgstr "פעולת ההצלה ‚${OPERATION}’ נכשלה עם קוד היציאה ${CODE}."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:8001
msgid "Execute a shell in ${DEVICE}"
msgstr "הרצת מעטפת בהתקן ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:9001
msgid "Execute a shell in the installer environment"
msgstr "להריץ מעטפת בסביבת ההתקנה"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:10001
msgid "Choose a different root file system"
msgstr "נא לבחור שורש אחר למערכת הקבצים"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:11001
msgid "Reboot the system"
msgstr "הפעלת המערכת מחדש"

#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001 ../rescue-mode.templates:16001
#: ../rescue-mode.templates:17001
msgid "Executing a shell"
msgstr "הרצת מעטפת"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:12001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"\". If you need any other file systems (such as a separate \"/usr\"), you "
"will have to mount those yourself."
msgstr ""
"לאחר הודעה זאת, תופיע מעטפת עם ${DEVICE} עם עיגון על „/”. אם נדרשות מערכות "
"קבצים אחרות (כגון „‎/usr” בנפרד), יש לעגן אותן באופן עצמאי."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid "Error running shell in /target"
msgstr "שגיאה בהרצת מעטפת ב־‎/target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:13001
msgid ""
"A shell (${SHELL}) was found on your root file system (${DEVICE}), but an "
"error occurred while running it."
msgstr ""
"נמצאה המעטפת (${SHELL}) על שורש מערכת הקבצים שלך בהתקן (${DEVICE}), אבל "
"התרחשה שגיאה בזמן ההרצה."

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No shell found in /target"
msgstr "לא נמצאה מעטפת (shell) ב־‎/target"

#. Type: error
#. Description
#. :sl3:
#: ../rescue-mode.templates:14001
msgid "No usable shell was found on your root file system (${DEVICE})."
msgstr ""
"לא נמצאה מעטפת בה ניתן להשתמש בשורש מערכת הקבצים (${DEVICE}) על ‎/target."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:15001
msgid "Interactive shell on ${DEVICE}"
msgstr "מעטפת אינטראקטיבית בהתקן ${DEVICE}"

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:16001
msgid ""
"After this message, you will be given a shell with ${DEVICE} mounted on \"/"
"target\". You may work on it using the tools available in the installer "
"environment. If you want to make it your root file system temporarily, run "
"\"chroot /target\". If you need any other file systems (such as a separate "
"\"/usr\"), you will have to mount those yourself."
msgstr ""
"לאחר הודעה זאת, תופיע מעטפת עם ${DEVICE} עם עיגון על „‎/target”. ניתן לעבוד "
"עליה עם הכלים הזמינים בסביבת תכנית ההתקנה. אם ברצונך לכוון אותה אל שורש "
"מערכת הקבצים באופן זמני, עליך להריץ את הפקודה „chroot /target”. אם נדרשות לך "
"מערכות קבצים אחרות (כגון „‎/usr” בנפרד), יש לעגן אותן באופן עצמאי."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:17001
msgid ""
"After this message, you will be given a shell in the installer environment. "
"No file systems have been mounted."
msgstr ""
"אחרי הודעה זאת, תופעל מעטפת עם הסביבה של תכנית ההתקנה. אף מערכת קבצים לא "
"עוגנה."

#. Type: text
#. Description
#. :sl3:
#: ../rescue-mode.templates:18001
msgid "Interactive shell in the installer environment"
msgstr "מעטפת אינטראקטיבית בסביבת ההתקנה"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Passphrase for ${DEVICE}:"
msgstr "מילת צופן עבור ${DEVICE}:"

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid "Please enter the passphrase for the encrypted volume ${DEVICE}."
msgstr "נא להקליד את מילת הצופן של הכרך המוצפן ${DEVICE}."

#. Type: password
#. Description
#. :sl3:
#: ../rescue-mode.templates:19001
msgid ""
"If you don't enter anything, the volume will not be available during rescue "
"operations."
msgstr "אם שדה זה יישאר ריק, הכרך לא יהיה זמין במהלך פעולות חילוץ."

#. Type: multiselect
#. Choices
#. :sl3:
#: ../rescue-mode.templates:20001
msgid "Automatic"
msgstr "אוטומטי"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid "Partitions to assemble:"
msgstr "מחיצות להרכבה:"

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Select the partitions to assemble into a RAID array. If you select "
"\"Automatic\", then all devices containing RAID physical volumes will be "
"scanned and assembled."
msgstr ""
"יש לבחור את המחיצות להרכבה לתוך מערך RAID. בחירה במצב „באופן אוטומטי”, תסרוק "
"ותרכיב כל ההתקנים המכילים כרכים פיזיים מסוג RAID."

#. Type: multiselect
#. Description
#. :sl3:
#: ../rescue-mode.templates:20002
msgid ""
"Note that a RAID partition at the end of a disk may sometimes cause that "
"disk to be mistakenly detected as containing a RAID physical volume. In that "
"case, you should select the appropriate partitions individually."
msgstr ""
"נא לשים לב שמחיצת RAID בסוף כונן עשויה לפעמים לגרום לזיהוי כוזב של הכונן "
"כאילו הוא מכיל כרך RAID פיזי. במקרה שכזה, יש לבחור את המחיצות המתאימות באופן "
"פרטני."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "Mount separate ${FILESYSTEM} partition?"
msgstr "לעגן מחיצה נפרדת ל־${FILESYSTEM}?"

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid "The installed system appears to use a separate ${FILESYSTEM} partition."
msgstr "נראה שהמערכת המותקנת משתמשת במחיצת ${FILESYSTEM} נפרדת."

#. Type: boolean
#. Description
#. :sl3:
#: ../rescue-mode.templates:21001
msgid ""
"It is normally a good idea to mount it as it will allow operations such as "
"reinstalling the boot loader. However, if the file system on ${FILESYSTEM} "
"is corrupt then you may want to avoid mounting it."
msgstr ""
"על פי רוב מומלץ לעגן אותה כיוון שמצב זה יאפשר פעולות כגון התקנת מערכת הטעינה "
"מחדש. עם זאת, אם מערכת הקבצים שב־${FILESYSTEM} פגומה אז יתכן שעדיף לך לא "
"לעגן אותה."
